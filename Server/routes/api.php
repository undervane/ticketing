<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api']], function () {

    Route::post('/register' , 'APIControllers\AuthController@register');
    Route::post('/login' , 'APIControllers\AuthController@login');
    Route::post('/tickets/create', 'APIControllers\TicketController@create');
    Route::get('/tickets/show', 'APIControllers\TicketController@show');
    Route::get('/tickets/delete', 'APIControllers\TicketController@delete');    

});

Route::group(['middleware' => ['auth:api']], function () {
    //Route::post('/create', 'APIControllers\YourController@index');
});