<?php

namespace App\Http\Middleware;

use Closure;

class parseToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $jwt_token = $request->cookie('jwt_token') ? $request->cookie('jwt_token') : $request->header('X-XSRF-TOKEN');
        if ( ! $jwt_token) {
            return $next($request);
        }

        $request->headers->set('Authorization', "Bearer {$jwt_token}");

        return $next($request);
    }
}
