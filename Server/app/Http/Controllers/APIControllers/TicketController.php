<?php

namespace App\Http\Controllers\APIControllers;

use App\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\RegistersUsers;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        
    public function create(Request $request) {
        
        auth()->shouldUse('api');

        $token = auth()->detectedToken();

        $user = auth()->findUserByToken($token);
        
        $result = Ticket::create([
            'message' => $request->message,
            'user' => $user->id,
        ]);       

        return response([
            'status' => 'success',
            'ticket' => $result] , 200
        );
    }

    public function show(Request $request) {

        auth()->shouldUse('api');

        $token = auth()->detectedToken();

        $user = auth()->findUserByToken($token);
        
        $ticket = Ticket::where('user', $user->id)
                    ->take(1)
                    ->get();    

        return response([
            'status' => 'success',
            'ticket' => $ticket->toArray()[0]] , 200
        );

    }

    public function delete(Request $request) {

        auth()->shouldUse('api');

        $token = auth()->detectedToken();

        $user = auth()->findUserByToken($token);
        
        $ticket = Ticket::where('user', $user->id)
                    ->delete();    

        return response([
            'status' => 'success'], 200
        );

    }


}
