import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConstantPool } from '@angular/compiler/src/constant_pool';
import { TicketingService } from '../../_services/ticketing.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public pending = false;
  public position = 5;
  public message: String = '';

  constructor(private ticketingService: TicketingService, private router: Router) {

  }

  ngOnInit() {

    this.ticketingService.show()
    .subscribe(res => {

      let parsed = JSON.parse(res._body);
    
      if (parsed.status){

        this.pending = true;
        this.position = parsed.position;
        this.message = parsed.ticket.message;

      }
      
    });

  }

  onCreateTicket(){

    this.ticketingService.create(this.message)
    .subscribe(res => {

      let parsed = JSON.parse(res._body);
    
      if (parsed.ticket){

        this.pending = true;
        this.position = parsed.position;
        this.message = parsed.message;

      }
      
    });

  }

  onDeleteTicket(){

    this.ticketingService.delete()
    .subscribe(res => {

      let parsed = JSON.parse(res._body);
    
      if (parsed.status == 'success'){

        this.pending = false;

      }
      
    });

  }

  setPending(){
    this.pending = true;
  }

  cancel(){
    this.pending = false;
  }

}
