import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TicketingService } from '../../_services/ticketing.service';

@Component({
  selector: 'app-hero-message',
  templateUrl: './hero-message.component.html',
  styleUrls: ['./hero-message.component.scss']
})
export class HeroMessageComponent implements OnInit {

  public position = 5;

  constructor(private router: Router) { }

  ngOnInit() {

  }

  cancel(){
    this.router.navigate(['home']);
  }

}
