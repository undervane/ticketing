import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../_services/authentication.service';
import { AuthGuard } from '../../_guards/auth.guard';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(
  	private authService: AuthenticationService, 
    private router: Router,
    private authGuard: AuthGuard,
  ) { }

  ngOnInit() {

  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  active(){

    return localStorage.getItem('user');

  }

}
