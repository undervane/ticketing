import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './_components/auth/login/login.component';
import { RegisterComponent } from './_components/auth/register/register.component';
import { HomeComponent } from './_components/home/home.component';
import { AuthGuard } from './_guards/auth.guard';
import { DashboardComponent } from './_components/dashboard/dashboard.component';
import { HeroMessageComponent } from './_components/hero-message/hero-message.component';

const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'pending', component: HeroMessageComponent },
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard]  },
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
    { path: '**', redirectTo: 'home' }
];

export const routing = RouterModule.forRoot(appRoutes);