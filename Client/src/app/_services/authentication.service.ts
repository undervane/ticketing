import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthenticationService {
    private headers: Headers;
    private readonly apiUrl = environment.apiUrl;

    constructor(private http: Http) {
        //append headers
        this.headers = new Headers();
        this.headers.append("Content-Type", 'application/json');
        this.headers.append("Access-Control-Allow-Origin", "*");
        this.headers.append("Access-Control-Allow-Headers", "Origin, Authorization, Content-Type, Accept");
        
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('user'));

    }

    login(email: string, password: string): Observable<any> {
        let request = JSON.stringify({ email: email, password: password });
        let options = new RequestOptions({ headers: this.headers }); // Create a request option
        return this.http.post(this.apiUrl+'/login', request, options)
            .pipe(
                map((response: Response) => {

                    if (response.json().status == 'success') {
                        // store email and jwt token in local storage to keep user logged in between page refreshes
                        localStorage.setItem('user', 
                            JSON.stringify({ 'session' : 'true' }));
                    }
                    return response;
                })
            );
    }

    register(username: string, email: string, password: string): Observable<any> {
        let request = JSON.stringify({ email: email, name: username, password: password });
        let options = new RequestOptions({ headers: this.headers }); // Create a request option
        return this.http.post(this.apiUrl+'/register', request, options)
            .pipe(
                map((response: Response) => {
                    if (response.json().status == 'success') {
                        localStorage.setItem('user', 
                            JSON.stringify({ 'session' : 'true' }));
                    }
                    return response;
                })
            );
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        localStorage.setItem('user', 
            JSON.stringify({ 'session' : 'false' }));
    }

}