import { Injectable } from '@angular/core';
import { Http, HttpModule, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { HttpClientModule } from '@angular/common/http';

@Injectable()
export class TicketingService {
    public token: string;
    private headers: Headers;
    private readonly apiUrl = environment.apiUrl;

    constructor(private http: Http) {

        var currentUser = JSON.parse(localStorage.getItem('user'));

        //append headers
        this.headers = new Headers();
        this.headers.append("Content-Type", 'application/json');
        this.headers.append("Accept", "application/json");
        
    }

    create(message: String): Observable<any> {

        let request = JSON.stringify({ message: message });
        let options = new RequestOptions({ headers: this.headers }); // Create a request option
        
        return this.http.post(this.apiUrl+'/tickets/create', request, options)
            .pipe(
                map((response: Response) => {
                    // register successful if there's a jwt token in the response
                    let error = response.json().error;
                    let user = localStorage.getItem("user");
                    if (!error) {
                        // store email and jwt token in local storage to keep user logged in between page refreshes
                        localStorage.setItem('user', 
                            JSON.stringify({ 'status' : 'success' }));
                    }
                return response;
            })
        );
    }

    index(): Observable<any> {

        let options = new RequestOptions({ headers: this.headers });

        return this.http.get(this.apiUrl+'/tickets', options)
            
            .pipe(
                map((response: Response) => {
                    localStorage.setItem('user', 
                            JSON.stringify({ 'status' : 'success' }));
                    return response;
                })
            );
    }
    
    show(): Observable<any>{

        let options = new RequestOptions({ headers: this.headers });

        console.log(`${this.apiUrl}/tickets/show`);

        return this.http.get(`${this.apiUrl}/tickets/show`, options)
            
            .pipe(
                map((response: Response) => {
                    localStorage.setItem('user', 
                            JSON.stringify({ 'status' : 'success' }));
                    return response;
                })
            );

    }

    delete(): Observable<any>{

        let options = new RequestOptions({ headers: this.headers });

        return this.http.get(`${this.apiUrl}/tickets/delete`, options)
            
            .pipe(
                map((response: Response) => {
                    localStorage.setItem('user', 
                            JSON.stringify({ 'status' : 'success' }));
                    return response;
                })
            );

    }

}
